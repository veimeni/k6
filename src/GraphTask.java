import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

      System.out.println(Integer.MAX_VALUE / 4);
      Graph g = new Graph ("Random");
      g.createRandomSimpleGraph (4, 4);

      System.out.println(g);

      Vertex rnd = g.getRandomVertex(); //find random vertex from graph
      System.out.println("Starting point: " + rnd);

      long before = System.currentTimeMillis();
      Vertex furthermost = g.getFurthermostVertex(rnd);
      long after = System.currentTimeMillis();

      System.out.println("End point: " + furthermost + ", distance "
              + furthermost.getVInfo() + " unit.");
      long time = after - before;
      System.out.println("ms: " + time);

      System.out.println("************This is my temporary handmade static test************");


      Graph t = new Graph("Singleton");
      Vertex v1 = new Vertex("v1");





//      Vertex v2 = new Vertex("v2");
//      Vertex v3 = new Vertex("v3");
//      Vertex v4 = new Vertex("v4");
//      Vertex v5 = new Vertex("v5");
//      Vertex v6 = new Vertex("v6");
      t.first = v1;
//      v1.next = v2;
//      v2.next = v3;
//      v3.next = v4;
//      v4.next = v5;
//      v5.next = v6;
//      Arc a1_4 = t.createArc("av1->v4", 1, v1, v4);
//      Arc a2_4 = t.createArc("av2->v4", 1, v2, v4);
//      Arc a3_2 = t.createArc("av3->v2", 1, v3, v2);
//      Arc a4_5 = t.createArc("av4->v5", 1, v4, v5);
//      Arc a5_2 = t.createArc("av5->v2", 1, v5, v2);
//      Arc a5_6 = t.createArc("av5->v6", 1, v5, v6);
//      Arc a6_1 = t.createArc("av6->v1", 1, v6, v1);
//      Arc a6_3 = t.createArc("av6->v3", 1, v6, v3);

      System.out.println(t);
      furthermost = t.getFurthermostVertex(v1);
      System.out.println("The furthermost vertex from '" + v1 + "' is '" + furthermost + "', distance "
              + furthermost.getVInfo() + " unit.");
//      furthermost = t.getFurthermostVertex(v2);
//      System.out.println("The furthermost vertex from '" + v2 + "' is '" + furthermost + "', distance "
//              + furthermost.getVInfo() + " unit.");
//      furthermost = t.getFurthermostVertex(v5);
//      System.out.println("The furthermost vertex from '" + v5 + "' is '" + furthermost + "', distance "
//              + furthermost.getVInfo() + " unit.");

   }


   /**
    * Construct a method to find the
    * furthermost vertex from given one
    * of connected simple graph .
    */
   class Vertex {

      private final String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      private void setVInfo(int num) {
         this.info = num;
      }

      private int getVInfo() {
         return this.info;
      }

      @Override
      public String toString() {
         return id;
      }

      private ArrayList<Arc> getAllArcs() {
         ArrayList<Arc> arcs = new ArrayList<>();

         arcs.add(this.first);
         Arc current = this.first;

         while (current.next != null) {
            current = current.next;
            arcs.add(current);
         }

         return arcs;
      }

      private ArcIterator<Arc> getArcIterator() {
         return new ArcIterator<>();
      }

      private class ArcIterator<Arc> implements Iterator<Arc> {

         private int index;

         public ArcIterator() {

            index = 0;
         }

         @Override
         public boolean hasNext() {

            return index < getAllArcs().size();
         }

         @Override
         public Arc next() {

            if (hasNext()) {
               index++;
               return (Arc) getAllArcs().get(index - 1);
            }
            return null;
         }
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private final String id;
      private Vertex target;
      private Arc next;
      private int info;

      Arc (String s, int i, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
         info = i;
      }

      Arc (String s, int i) {
         this (s, i, null, null);
      }

      private int getInfo() {
         return this.info;
      }

      private Vertex getToVert() {
         return this.target;
      }

      @Override
      public String toString() {
         return id;
      }

   }

   class Graph {

      private final String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      private int getRandomArcLength() {
         Random random = new Random();
         return random.nextInt(10) + 1;
      }

      private ArrayList<Vertex> getAllVertices() {
         ArrayList<Vertex> vertices = new ArrayList<>();

         vertices.add(this.first);
         Vertex current = this.first;

         while (current.next != null) {
            current = current.next;
            vertices.add(current);
         }

         return vertices;
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (": ");
               sb.append (a.getInfo());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Create a Vertex.
       * @param vid String id of Vertex
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Create an arc.
       * Each arc connects two vertices.
       * @param aid String id of arc
       * @param from Vertex where arc is leaving from
       * @param to Vertex destination point of arc
       */
      public Arc createArc (String aid, int info, Vertex from, Vertex to) {
         Arc res = new Arc (aid, info);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int randomLength = getRandomArcLength();
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), randomLength,
                       varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), randomLength,
                       varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            int arcLength = getRandomArcLength();
            createArc ("a" + vi.toString() + "_" + vj.toString(),
                    arcLength, vi, vj);
            connected [i][j]++;
            createArc ("a" + vj.toString() + "_" + vi.toString(),
                    arcLength, vj, vi);
            connected [j][i]++;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Returns random Vertex from graph.
       */
      public Vertex getRandomVertex() {
         Random random = new Random();
         int randId = random.nextInt(this.info - 1) + 1;
         Vertex v = this.first;

         while (v.info != randId) {
            v = v.next;
         }
         return v;
      }

      /**
       * Shortest paths from a given vertex. Uses Dijkstra's algorithm.
       * For each vertex vInfo is length of shortest path from given
       * source s and vObject is previous vertex from s to this vertex.
       * @param s source vertex
       */
      public void shortestPathsFrom (Vertex s) {
         if (getAllVertices().size() == 0) return;
         if (!getAllVertices().contains (s))
            throw new RuntimeException ("wrong argument to Dijkstra!!");
         int INFINITY = Integer.MAX_VALUE / 4; // big enough!!!

         Iterator vit = new VertexIterator();
         while (vit.hasNext()) {
            Vertex v = (Vertex)vit.next();
            v.setVInfo (INFINITY);
         }
         s.setVInfo (0);

         List vq = Collections.synchronizedList (new LinkedList());
         vq.add (s);

         while (vq.size() > 0) {
            int minLen = INFINITY;
            Vertex minVert = null;
            Iterator it = vq.iterator();

            while (it.hasNext()) {
               Vertex v = (Vertex)it.next();

               if (v.getVInfo() < minLen) {
                  minVert = v;
                  minLen = v.getVInfo();
               }
            }
            if (minVert == null)
               throw new RuntimeException ("error in Dijkstra!!");
            if (vq.remove (minVert)) {
               // minimal element removed from vq
            } else
               throw new RuntimeException ("error in Dijkstra!!");

            it = minVert.getArcIterator();
            while (it.hasNext()) {
               Arc a = (Arc)it.next();
               int newLen = minLen + a.getInfo();

               Vertex to = a.getToVert();
               if (to.getVInfo() == INFINITY) {
                  vq.add (to);
               }
               if (newLen < to.getVInfo()) {
                  to.setVInfo (newLen);
               }
            }
         }

      }

      /**
       * Find the furthermost vertex from given one.
       * @param start source vertex
       * @return furthermost vertex
       */
      public Vertex getFurthermostVertex(Vertex start) {
         if (getAllVertices().contains (start) && getAllVertices().size() == 1) {
            return start;
         }
         this.shortestPathsFrom(start);
         Vertex furthermost = start;
         Iterator vit = new VertexIterator();
         while (vit.hasNext()) {
            Vertex v = (Vertex)vit.next();
            if (v.getVInfo() > furthermost.getVInfo()) {
               furthermost = v;
            }
         }
         return furthermost;
      }

      private class VertexIterator<Vertex> implements Iterator<Vertex> {

         private int index;

         public VertexIterator() {

            index = 0;
         }

         @Override
         public boolean hasNext() {
            return index < getAllVertices().size();
         }

         @Override
         public Vertex next() {

            if (hasNext()) {
               index++;
               return (Vertex) getAllVertices().get(index - 1);
            }
            return null;
         }
      }
   }

} 

